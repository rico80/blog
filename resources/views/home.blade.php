@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <card titulo="Dashboard">
                <div class="row">
                    <div class="col-md-4">
                        <card titulo="Conteudo 1">
                            Teste de conteudo
                        </card>
                    </div>
                    <div class="col-md-4">
                        <card titulo="Conteudo 2">
                            Teste de conteudo
                        </card>
                    </div>
                    <div class="col-md-4">
                        <card titulo="Conteudo 3">
                            Teste de conteudo
                        </card>
                    </div>
                </div>
            </card>
        </div>
    </div>
</div>
@endsection
